/*Tehtävänannon taustaa ja ohjeita:
Määrittele yliluokka Person, joka sisältää ihmisen henkilötietoja:

etunimet,
sukunimi,
kutsumanimi,
syntymävuosi,
kuolinvuosi*/

class Person {
    constructor(firstname, lastname, nickname, born, died){
        this.firstname = firstname;
        this.lastname = lastname;
        this.nickname = nickname
        this.born = born || 0;
        this.died = died || 0;
    }

    setYearBorn(year) {
        this.born = year;
        return true
    }
    getYearBorn() {
        return this.born;
    }

    setYearDied(year) {
        this.died = year;
        return true
    }
    getYearDied() {
        return this.died;
    }
}

module.exports = Person;