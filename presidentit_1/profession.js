/*Tehtävänantoon taustaa. 
Profession, joka perii Person-luokan ja toteuttaa lisäksi saantifunktiot (get- ja set-) 
Profession-luokalle merkityksellisiin attribuutteihin. Lisää Profession luokkaan seuraavat ominaisuudet:
linkki kuvaan,
virka alkoi (vuosiluku),
virka päättyi (vuosiluku),
linkki historiikkiin.*/

var Person = require('./person.js');

class Profession extends Person {
    constructor(firstname, lastname, nickname, born, died, link, started, ended, picture){
        super(firstname, lastname, nickname, born, died)
        this.link = link;
        this.started = started;
        this.ended = ended;
        this.picture = picture || 0;
    }
}

/* Tehtävänannossa käytettävät linkit: 'https://suomenpresidentit.fi/kallio/'
'https://suomenpresidentit.fi/pof/wp-content/uploads/2017/02/kallio-20-l-alussa.jpg'*/

let pressa = new Profession("Kyösti", "Kallio", "Köpi", "1873", "1940", 'https://suomenpresidentit.fi/kallio/', 1937, 1940,'https://suomenpresidentit.fi/pof/wp-content/uploads/2017/02/kallio-20-l-alussa.jpg'  )
console.log(pressa.firstname)
console.log(pressa.lastname)
console.log(pressa.nickname)
console.log(pressa.born)
console.log(pressa.died)
console.log(pressa.link)
console.log(pressa.started)
console.log(pressa.ended)
console.log(pressa.picture)