/*Tehtävänanto
1) Toteuta JavaScript-ohjelma, joka vastaanottaa käyttäjien viestit ja kaiuttaa ne takaisin kaikille käyttäjille. Käytä TCP-protokollaa.

2) Testaa sovelluksen toimivuus esimerkiksi usealla Telnet-yhteydellä.

3) Toteuta yksinkertainen viestisysteemi, jossa palvelin ylläpitää yhteyden tilaa ja tottelee komentoja:

HELP: Tulosta mahdollinen komentolistaus
NICK: Muuta istunnon nicknamea
MSG: Lähetä yksityisviesti käyttäjälle
WHOIS: Näytä käyttäjän tietoja (mieti mistä tietoja saadaan ja mitä on järkevä näyttää)*/

const net = require('net');

var connections = [];

net.createServer(function(socket) {

    socket.setEncoding('utf8');    // Pura Buffer selkokieliseksi
    connections.push(socket);

    console.log("new connection established:", connections.length, "user(s) connected");

    socket.nick = "user" + (Math.random() * 1000000).toFixed();
    socket.write("Tervetuloa käyttäjä " + socket.nick + "\n");

    socket.on('data', function(buffer) {
        console.log("received data", buffer);
        console.log("received data", buffer.toString());
        console.log("received data length", buffer.toString().length);
        console.log("trimmed data length", buffer.toString().trim().length);

        var command = buffer.toString().trim().split(" ");
        console.log(command);

        switch(command[0]) {
            case 'HELP':
                console.log("HELP RECEIVED");
                socket.write("Commands are: HELP, NICK, MSG, WHOIS");
                break;

            case 'NICK':
                console.log("NICK RECEIVED", command[1]);
                this.nick = command[1];
                socket.write("Nickkisi on nyt " + this.nick + "\n");
                break;

            case 'MSG':
                var msg = command.slice(2).join(" ");
                console.log("MSG RECEIVED", msg);
                connections.forEach(function(user) {
                    if(user.nick == command[1]) {
                        user.write(socket.nick + " sanoo: " + msg + "\n");
                    }
                });
                break;

            case 'WHOIS':
                connections.forEach(function(user) {
                    if(user.nick == command[1]) {
                        socket.write(user.nick + " : " + JSON.stringify(user.address()) + "\n");
                        console.log(user.address());
                        console.log(JSON.stringify(user.address()));
                    }
                });
                break;

            default:
                console.log("Command not found:", command);
        }
    })

}).listen(8000);
console.log("palvelin on nyt käynnistetty");
console.log("yhteys telnetillä 'telnet localhost 8000'");
