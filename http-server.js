var express = require('express');

const PORT = 8000;
const HOST = '127.0.0.1';

var app = express();

app.get('/', function(req, res) {
  console.log("server is handling the request");
  res.send("juurihakemisto on tyhjä");
});

app.get('/welcome', function(req, res) {
  console.log("return welcome message");
  res.send("hello world!");
})
app.listen(PORT, HOST, function() {
  console.log("server up and running @ http://" + HOST + ":" + PORT);
});