var Mediaplayer = function () {
    console.log("Mediaplayer started");

    var player = document.getElementById("audio2");
    var remainingTimeDisplay = document.getElementById("remainingTime");

    initMediaplayer();

    async function getSongs() {
        const response = await fetch("http://localhost:8000/api/v1/songs");
        const biisit = await response.json();
        console.log(biisit);
        return biisit;
    }

    async function renderPlaylist() {
        let songs = await getSongs();
        
        var soittolista = "<table>";    // Taulukon aloitustagi
        soittolista += "<tr><th>Biisin nimi</th><th>Id</th><th>Kesto</th></tr>";

        for (var song in songs) {
            var audio = new Audio();
            audio.src = songs[song];
            audio.songId = song;

            soittolista += '<tr class="selectable" onclick="myPlayer.selectSong(\'' + songs[song] + '\')">';
            soittolista += '<td>' + songs[song] + '</td>';
            soittolista += '<td>' + song + '</td>';
            soittolista += '<td id="song' + song + '">hh:mm:ss</td>';
            soittolista += '</tr>';

            audio.addEventListener('loadedmetadata', function() {
                console.log("loaded duration", this.duration);
                
                let seconds = this.duration;
                const result = new Date(seconds * 1000).toISOString().slice(11, 19);
                console.log(result); // "00:10:00" (hh:mm:ss)

                document.getElementById('song' + this.songId).innerHTML = result;
            });
        }

        soittolista += "</table>";   // Taulukon lopetustagi

        // Sijoita edellä kasattu biisilista oikeaan div-elementtiin
        document.getElementById('playList').innerHTML = soittolista;
    }

    function initMediaplayer() {
        console.log("initMediaplayer called");

        document.getElementById("btnPlay").addEventListener("click", playMusic, false);
        document.getElementById("btnStop").addEventListener("click", stopMusic, false);
        document.getElementById("btnPause").addEventListener("click", pauseMusic, false);
        document.getElementById("btnVolDown").addEventListener("click", volumeDown, false);
        document.getElementById("btnVolUp").addEventListener("click", volumeUp, false);
        document.getElementById("btnForward").addEventListener("click", forward, false);
        document.getElementById("btnBackward").addEventListener("click", backward, false);

        player.addEventListener("timeupdate", updateRemainingTime);

        renderPlaylist();
    }

    this.selectSong = function(song) {
        console.log("selected", song);
        player.src = song;
        player.play();
    }

    function playMusic() {
        console.log("playMusic");
        player.play();
    }

    function stopMusic() {
        console.log("stopMusic");
        player.pause();
        player.currentTime = 0;
        updateRemainingTime();
    }

    function pauseMusic() {
        console.log("pauseMusic");
        player.pause();
    }

    function volumeDown() {
        console.log("volumeDown");
        if (player.volume > 0) {
            player.volume -= 0.1;
            player.volume = parseFloat(player.volume.toFixed(1));
        }
        console.log("current volume", player.volume);
    }

    function volumeUp() {
        console.log("volumeUp");
        if (player.volume < 1) {
            player.volume += 0.1;
            player.volume = parseFloat(player.volume.toFixed(1));
        }
        console.log("current volume", player.volume);
    }

    function forward() {
        console.log("forward");
        player.currentTime += 10; // Forward 10s.
        updateRemainingTime();
    }

    function backward() {
        console.log("backward");
        player.currentTime -= 10; // Backward 10s.
        if (player.currentTime < 0) {
            player.currentTime = 0;
        }
        updateRemainingTime();
    }

    function updateRemainingTime() {
        if (!isNaN(player.duration) && !isNaN(player.currentTime)) {
            const remainingTime = player.duration - player.currentTime;
            const result = new Date(remainingTime * 1000).toISOString().slice(11, 19);
            remainingTimeDisplay.innerHTML = "Remaining time: " + result;
        } else {
            remainingTimeDisplay.innerHTML = "Remaining time: 00:00";
        }
    }
}

// Luo Mediaplayer-olio ja aseta se muuttujaan, jotta se on globaalisti käytettävissä
var myPlayer = new Mediaplayer();

