var express = require('express'); // versio 14.x
//import express from 'express'; // versio 16.xx

var songs = ["https://opengameart.org/sites/default/files/Soliloquy_1.mp3", 
                "https://opengameart.org/sites/default/files/Sigil_3.ogg",
                "https://opengameart.org/sites/default/files/technophobic%20android-temple%20of%20the%20maker.mp3"]

const PORT = 8000;
const HOST = '0.0.0.0';

var app = express();

app.use(express.static('musiikkisoitin'));

/*
app.get('/', function(req, res) {
  res.send("hello world!");
})
*/

app.get('/api/v1/songs', function(req, res){
    console.log("/api/v1/songs kutsuttu");
    res.json(songs);
});

app.listen(PORT, HOST, function() {
  console.log("palvelin käynnistetty!");
});